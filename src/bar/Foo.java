package bar;

import java.util.Arrays;
import java.util.List;

public class Foo {
    public String hello(boolean param) {
        List<String> list = Arrays.asList("1", "2", "hello");
        if (param)
            return "default";
        return list.stream().filter(x -> x.equalsIgnoreCase("hello")).findFirst().get();
    }
}
