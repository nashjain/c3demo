<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<head>
				<title>Dead Code Report</title>
			</head>
			<body>
				<h2>Potential Dead Code in your Code Base</h2>
				<table>
					<tr bgcolor="#9acd32">
						<th align="left">Type</th>
						<th align="left">Classname</th>
						<th align="left">In</th>
					</tr>
					<xsl:for-each select="dcd/publicDeadCode">
						<tr>
							<td>
								Unused Public Methods
							</td>
							<td>
								<xsl:value-of select="@className" />
							</td>
							<td>
								<xsl:value-of select="substring-before(substring-after(text(), '['), ']')" />
							</td>
						</tr>
					</xsl:for-each>

					<xsl:for-each select="dcd/privateDeadCode">
						<tr>
							<td>
								Unused Private Methods
							</td>
							<td>
								<xsl:value-of select="@className" />
							</td>
							<td>
								<xsl:value-of select="substring-before(substring-after(text(), '['), ']')" />
							</td>
						</tr>
					</xsl:for-each>

					<xsl:for-each select="dcd/uselessInitialization">
						<tr>
							<td>
								Useless initializations in classes
							</td>
							<td>
								<xsl:value-of select="@className" />
							</td>
							<td>
								<xsl:value-of select="substring-before(substring-after(text(), '['), ']')" />
							</td>
						</tr>
					</xsl:for-each>

					<xsl:for-each select="dcd/deadLocalVariable">
						<tr>
							<td>
								Unused Local Variables
							</td>
							<td>
								<xsl:value-of select="@className" />
							</td>
							<td>
								<xsl:value-of select="substring-before(substring-after(text(), '['), ']')" />
							</td>
						</tr>
					</xsl:for-each>
					<xsl:for-each select="dcd/selfAssignment">
						<tr>
							<td>
								Self assignments
							</td>
							<td>
								<xsl:value-of select="@className" />
							</td>
							<td>
								<xsl:value-of select="substring-before(substring-after(text(), '['), ']')" />
							</td>
						</tr>
					</xsl:for-each>
					<xsl:for-each select="dcd/stringToString">
						<tr>
							<td>
								Call of toString() on String
							</td>
							<td>
								<xsl:value-of select="@className" />
							</td>
							<td>
								<xsl:value-of select="substring-before(substring-after(text(), '['), ']')" />
							</td>
						</tr>
					</xsl:for-each>

				</table>
				<h3>Summary</h3>
				Duration:
				<xsl:value-of select="dcd/summary/@durationMillis" />
				<br />
				Total Suspect Count:
				<xsl:value-of select="dcd/summary/@suspectCount" />
				<br />
				Total No. of Classes Analyzed:
				<xsl:value-of select="dcd/summary/@analyzedClassCount" />
				<br />
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>